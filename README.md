# APP Biblioteca

## Instalação

1. Faça o clone deste projeto com `git clone https://MarcioFernando@bitbucket.org/MarcioFernando/biblioteca-app.git`
2. Entre na pasta do projeto e instale as dependências com `npm install`

## Inicializando a aplicação

Na pasta do projeto digite o seguinte comando para iniciar a aplicação `npm start`. 
A aplicação roda sobre o seguinte endereço `http://localhost:3000/`.

## Endereços

### Administração

`http://localhost:3000/admin`

### Usuário final

`http://localhost:3000/`